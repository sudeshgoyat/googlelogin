import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
name:any;
email:any;
token:any;

  constructor(public navCtrl: NavController,
  private storage: Storage) {
   this. storage.get('name').then((data)=>{
     this.name=data;
   });
    this. storage.get('email').then((data)=>{
     this.email=data;
   });
     this. storage.get('token').then((data)=>{
     this.token=data;
   });
  }

}
