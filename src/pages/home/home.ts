import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Storage } from '@ionic/storage';
import { ContactPage } from "../contact/contact";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
userInfo:any;
isUserLoggedIn:any;
  constructor(public navCtrl: NavController,
  private gp: GooglePlus,private storage: Storage) {
 
  }
loginWithGP(){
 this.gp.login({}).then(res=>{
   //alert(JSON.stringify(res))
   this.userInfo=res;
   this.isUserLoggedIn=true;
   this. storage.set('name', res.displayName);
    this. storage.set('email', res.email);
     this. storage.set('token', res.accessToken);
   }).catch( err => console.log(err));
}
logout(){
 this.gp.logout().then( ()=>{
   this.isUserLoggedIn=false;
 });
}

move(){
  this.navCtrl.push(ContactPage)
}
}
